//
// workgroup - UD Cluster utilities, workgroup helper
// WorkdirMap.h
//
// Public interfaces to the Workgroup directory mapping
// functionality.
//
// Copyright (c) 2011
// Dr. Jeffrey Frey
// Network & Systems Services, University of Delaware
//

#ifndef __WORKDIRMAP_H__
#define __WORKDIRMAP_H__

#include "config.h"

typedef struct __WorkdirMap * WorkdirMapRef;

WorkdirMapRef WorkdirMapRetain(WorkdirMapRef aMap);
void WorkdirMapRelease(WorkdirMapRef aMap);

/* */

WorkdirMapRef WorkdirMapCreateStatic(const char *basePath);

/* */

#ifdef HAVE_LIBYAML
WorkdirMapRef WorkdirMapCreateYAML(const char *yamlFile);
#endif

/* */

typedef struct __WorkdirMapStack * WorkdirMapStackRef;

WorkdirMapStackRef WorkdirMapStackGetDefault(void);

WorkdirMapStackRef WorkdirMapStackCreate(void);
WorkdirMapStackRef WorkdirMapStackRetain(WorkdirMapStackRef aMapStack);
void WorkdirMapStackRelease(WorkdirMapStackRef aMapStack);

bool WorkdirMapStackPush(WorkdirMapStackRef aMapStack, WorkdirMapRef aMap);

const char* WorkdirMapStackDirectoryForGroup(WorkdirMapStackRef aMapStack, gid_t gidNumber, const char *gidName);

#endif /* __WORKDIRMAP_H__ */
