//
// workgroup - UD Cluster utilities, workgroup helper
// config.c
//
// Program-wide includes, etc.
//
// Copyright (c) 2011
// Dr. Jeffrey Frey
// Network & Systems Services, University of Delaware
//
// $Id: config.c 367 2011-11-28 05:19:52Z frey $
//

#include "config.h"

#ifndef HAVE_ASPRINTF

int
asprintf(
  char        **ret,
  const char  *format,
  ...
)
{
  va_list     argv;
  int         bytes;
  
  va_start(argv, format);
  bytes = vsnprintf(NULL, 0, format, argv);
  va_end(argv);
  
  if ( bytes >= 0 ) {
    char*     buffer = malloc(1 + bytes);
    
    if ( buffer ) {
      va_start(argv, format);
      bytes = vsnprintf(buffer, bytes + 1, format, argv);
      *ret = buffer;
      va_end(argv);
    }
  }
  return bytes;
}
     
#endif

//

#ifndef HAVE_STRDUP

char*
strdup(
  const char* s
)
{
  if ( s ) {
    char*     S = malloc(strlen(s) + 1);
    strcpy(S, s);
    return S;
  }
  return NULL;
}

#endif

//

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

bool
workgroup_is_file(
  const char    *path
)
{
  struct stat   pathInfo;
  
  if ( stat(path, &pathInfo) == 0 ) {
    if ( S_ISREG(pathInfo.st_mode) ) return true;
  }
  return false;
}

//

#include <pwd.h>

const char*
workgroup_path_relative_to_homedir(
  const char    *path
)
{
  struct passwd *passwd_record = getpwuid(getuid());
  
  if ( passwd_record && passwd_record->pw_dir && (*(passwd_record->pw_dir) == '/') ) {
    char        *fullPath = NULL;
    
    asprintf(&fullPath, "%s/%s", passwd_record->pw_dir, path);
    return (const char*)fullPath;
  }
  return NULL;
}
