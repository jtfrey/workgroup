//
// workgroup - UD Cluster utilities, workgroup helper
// workdir-main.c
//
// Main entry point for the utility.
//
// Copyright (c) 2011
// Dr. Jeffrey Frey
// Network & Systems Services, University of Delaware
//

#include "WorkgroupList.h"

//

#include <locale.h>
#include <getopt.h>

static struct option cliOptions[] = {
  { "group",     required_argument,  NULL,           'g' },
  { "help",      no_argument,        NULL,           'h' },
  { "version",   no_argument,        NULL,           'V' },
  { NULL,        0,                  NULL,            0  }
};

void
usage(
  char*     exeName
)
{
  printf(
      "\n"
      " Display an HPC workgroup's work directory.\n\n"
      "  %s {options}\n\n"
      " Options:\n\n"
      "  --help/-h                this information\n"
      "  --version/-V             show the program version\n\n"
      "  --group/-g <gid/name>    show workdir for the given workgroup\n"
      "                           (default:  your current group)\n\n"
#ifdef WORKGROUP_DEFAULT_MAPFILE
      " * Non-static directory mappings read from " WORKGROUP_DEFAULT_MAPFILE "\n\n"
#endif
#ifdef ENABLE_USER_MAPFILE
      " * User directory mapping overrides will be read from ~/.workdirs.yaml\n\n"
#endif
      ,
      exeName
    );
}

//

int
main(
  int       argc,
  char*     argv[]
)
{
  int                   rc = 0;
  WorkgroupListRef      wgList = NULL;
  char*                 exeName = argv[0];
  char                  whichOption;
  char*                 gidStr = NULL;

  setlocale(LC_ALL, "");
  
  //
  // Check for CLI arguments:
  //
  while ( (whichOption = getopt_long(argc, argv, "g:hV", cliOptions, NULL)) != -1) {
    switch (whichOption) {
      
      case 'g': {
        gidStr = optarg;
        break;
      }
      
      case 'h': {
        usage(exeName);
        exit(0);
      }

      case 'V': {
        printf(
            "%s\n"
            "(built %s %s)\n",
            WORKGROUP_VERSION_STRING,
            __DATE__, __TIME__
          );
        exit(0);
      }
      
    }
  }
  // Skip-over all the CLI stuff that getopt processed:
  argc -= optind;
  argv += optind;
  
  wgList = WorkgroupListCreate();
  if ( wgList ) {
    if ( ! gidStr ) {
     struct group*  curGroup = getgrgid(getgid());
     
     if ( curGroup && (curGroup->gr_gid >= WORKGROUP_GID_LOW) ) {
       gidStr = curGroup->gr_name;
      }
    }
    if ( ! gidStr ) {
      fprintf(stderr, "ERROR:  no workgroup provided and current Unix group is not a workgroup\n");
      rc = EACCES;
    } else {
      WorkgroupRef    wg = WorkgroupListGetWorkgroupByCString(wgList, gidStr);
      
      if ( wg ) {
        //
        // All was okay, show work directory:
        //
        const char*   workdir = WorkgroupGetWorkDirectory(wg);
        
        if ( workdir )
          printf("%s\n", workdir);
      } else {
        printf("ERROR:  You are not a member of workgroup '%s'\n", gidStr);
        rc = EINVAL;
      }
    }
    WorkgroupListFree(wgList);
  } else {
    fprintf(stderr, "You are not a member of any workgroups.\n");
    rc = EACCES;
  }
  return rc;
}
