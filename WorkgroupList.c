//
// workgroup - UD Cluster utilities, workgroup helper
// WorkgroupList.h
//
// Implementation of the WorkgroupList pseudo-class.
//
// Copyright (c) 2011
// Dr. Jeffrey Frey
// Network & Systems Services, University of Delaware
//
// $Id: WorkgroupList.c 372 2011-11-28 20:00:05Z frey $
//

#include "WorkgroupList.h"

#ifndef WORKGROUP_GID_LOW
#define WORKGROUP_GID_LOW (0)
#endif

#ifndef WORKGROUP_GID_HIGH
#define WORKGROUP_GID_HIGH (999999)
#endif

//

typedef struct _WorkgroupList {
  unsigned int  count;
  WorkgroupRef* workgroups;
  ITEM**        cursesMenuItems;
  MENU*         cursesMenu;
} WorkgroupList;

//

WorkgroupListRef
WorkgroupListCreate()
{
  WorkgroupList*    newList = NULL;
  gid_t             gidList[NGROUPS_MAX];
  int               gidCount = getgroups(NGROUPS_MAX, gidList);
  
  if ( gidCount > 0 ) {
    newList = (WorkgroupList*)malloc(sizeof(WorkgroupList) + gidCount * sizeof(WorkgroupRef) + (gidCount + 1) * sizeof(ITEM*));
    if ( newList ) {
      int           wgIdx = 0, gidIdx = 0;
      
      newList->workgroups = (void*)newList + sizeof(WorkgroupList);
      newList->cursesMenuItems = (void*)newList->workgroups + gidCount * sizeof(WorkgroupRef);
      newList->cursesMenu = NULL;
      while ( gidIdx < gidCount ) {
        if ( (gidList[gidIdx] >= WORKGROUP_GID_LOW) && (gidList[gidIdx] <= WORKGROUP_GID_HIGH) ) {
          WorkgroupRef    wg = WorkgroupCreateWithGidNumber(gidList[gidIdx]);
          
          if ( wg ) {
            // Insert into the list, ordered by group name:
            int           insIdx = 0;
            const char*   newWgName = WorkgroupGetGidString(wg);
            
            while ( insIdx < wgIdx ) {
              if ( strcasecmp(newWgName, WorkgroupGetGidString(newList->workgroups[insIdx])) < 0 )
                break;
              insIdx++;
            }
            // If insIdx < wgIdx, then we have some records to shift:
            if ( insIdx < wgIdx )
              memmove(&newList->workgroups[insIdx + 1], &newList->workgroups[insIdx], (wgIdx - insIdx) * sizeof(WorkgroupRef));
            newList->workgroups[insIdx] = wg;
            wgIdx++;
          }
        }
        gidIdx++;
      }
      if ( (newList->count = wgIdx) == 0 ) {
        free(newList);
        newList = NULL;
      }
    }
  }
  return (WorkgroupListRef)newList;
}

//

void
WorkgroupListFree(
  WorkgroupListRef    aWorkgroupList
)
{
  int               wgIdx = 0;
  
  while ( wgIdx < aWorkgroupList->count ) {
    WorkgroupFree(aWorkgroupList->workgroups[wgIdx]);
    if ( aWorkgroupList->cursesMenu )
      free_item(aWorkgroupList->cursesMenuItems[wgIdx]);
    wgIdx++;
  }
  if ( aWorkgroupList->cursesMenu )
    free_menu(aWorkgroupList->cursesMenu);
  free(aWorkgroupList);
}

//

unsigned int
WorkgroupListGetCount(
  WorkgroupListRef    aWorkgroupList
)
{
  return aWorkgroupList->count;
}

//

WorkgroupRef
WorkgroupListGetWorkgroupAtIndex(
  WorkgroupListRef    aWorkgroupList,
  unsigned int        index
)
{
  if ( index < aWorkgroupList->count )
    return aWorkgroupList->workgroups[index];
  return NULL;
}

//

MENU*
WorkgroupListGetCursesMenu(
  WorkgroupListRef    aWorkgroupList
)
{
  if ( ! aWorkgroupList->cursesMenu ) {
    unsigned int    wgIdx = 0;
    
    while ( wgIdx < aWorkgroupList->count ) {
      WorkgroupRef  wg = aWorkgroupList->workgroups[wgIdx];
      
      aWorkgroupList->cursesMenuItems[wgIdx] = new_item(
                                                    WorkgroupGetGidNumberString(wg),
                                                    WorkgroupGetGidString(wg)
                                                  );
      wgIdx++;
    }
    aWorkgroupList->cursesMenuItems[wgIdx++] = NULL;
    aWorkgroupList->cursesMenu = new_menu(aWorkgroupList->cursesMenuItems);
    if ( aWorkgroupList->cursesMenu ) {
      set_menu_mark(aWorkgroupList->cursesMenu, "");
      set_menu_opts(aWorkgroupList->cursesMenu, O_SHOWDESC | O_ONEVALUE);
    } else {
      wgIdx = 0;
      while ( wgIdx < aWorkgroupList->count )
        free_item(aWorkgroupList->cursesMenuItems[wgIdx++]);
    }
  }
  return aWorkgroupList->cursesMenu;
}

//

WorkgroupRef
WorkgroupListGetSelectedWorkgroupFromMenu(
  WorkgroupListRef  aWorkgroupList
)
{
  if ( aWorkgroupList->cursesMenu ) {
    unsigned int  wgIdx = 0;
    
    while ( wgIdx < aWorkgroupList->count ) {
      if ( aWorkgroupList->cursesMenuItems[wgIdx] == aWorkgroupList->cursesMenu->curitem ) {
        return aWorkgroupList->workgroups[wgIdx];
      }
      wgIdx++;
    }
  }
  return NULL;
}

//

WorkgroupRef
WorkgroupListGetWorkgroupByCString(
  WorkgroupListRef  aWorkgroupList,
  const char*       gidString
)
{
  WorkgroupRef      wg = WorkgroupListGetWorkgroupByName(aWorkgroupList, gidString);
  
  if ( ! wg ) {
    char*           endptr = NULL;
    gid_t           gidNumber = strtol(gidString, &endptr, 10);
        
    // Try by number:
    if ( endptr && (*endptr == '\0') ) {
      wg = WorkgroupListGetWorkgroupByGidNumber(aWorkgroupList, gidNumber);
    }
  }
  return wg;
}

//

WorkgroupRef
WorkgroupListGetWorkgroupByGidNumber(
  WorkgroupListRef  aWorkgroupList,
  gid_t             gidNumber
)
{
  //
  // Workgroups are sorted by string, not gidNumber, so we have to check 'em all
  //
  int               wgIdx = 0;
  
  while ( wgIdx < aWorkgroupList->count ) {
    if ( WorkgroupGetGidNumber(aWorkgroupList->workgroups[wgIdx]) == gidNumber )
      return aWorkgroupList->workgroups[wgIdx];
    wgIdx++;
  }
  return NULL;
}

//

int
__WorkgroupListBSearchByNameComparator(
  const void*       A,
  const void*       B
)
{
  const char*       k = (const char*)A;
  const char*       b = WorkgroupGetGidString(*((WorkgroupRef*)B));
  
  return strcasecmp(k, b);
}

WorkgroupRef
WorkgroupListGetWorkgroupByName(
  WorkgroupListRef  aWorkgroupList,
  const char*       gidName
)
{
  //
  // We recogize a "dot" to imply the current egid:
  //
  if ( strcmp(gidName, ".") == 0 )
    return WorkgroupListGetWorkgroupByGidNumber(aWorkgroupList, getegid());

  //
  // Workgroups are sorted by string so we can do a binary search:
  //
  WorkgroupRef*     foundWG = bsearch(
                                  gidName,
                                  aWorkgroupList->workgroups,
                                  aWorkgroupList->count,
                                  sizeof(WorkgroupRef),
                                  __WorkgroupListBSearchByNameComparator
                                );
  if ( foundWG )
    return *foundWG;
  return NULL;
}
