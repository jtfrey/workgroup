//
// workgroup - UD Cluster utilities, workgroup helper
// Workgroup.h
//
// Public interfaces to the Workgroup pseudo-class.
//
// Copyright (c) 2011
// Dr. Jeffrey Frey
// Network & Systems Services, University of Delaware
//
// $Id: Workgroup.h 575 2015-10-29 19:54:23Z frey $
//

#ifndef __WORKGROUP_H__
#define __WORKGROUP_H__

#include "config.h"

typedef struct _Workgroup* WorkgroupRef;

WorkgroupRef WorkgroupCreateWithGidNumber(gid_t gidNumber);
void WorkgroupFree(WorkgroupRef aWorkgroup);

gid_t WorkgroupGetGidNumber(WorkgroupRef aWorkgroup);
const char* WorkgroupGetGidNumberString(WorkgroupRef aWorkgroup);
const char* WorkgroupGetGidString(WorkgroupRef aWorkgroup);
const char* WorkgroupGetWorkDirectory(WorkgroupRef aWorkgroup);

int WorkgroupSpawnSubshell(WorkgroupRef aWorkgroup, bool setWorkingDirectory, bool fixupUmask);
int WorkgroupExecCommand(WorkgroupRef aWorkgroup, bool setWorkingDirectory, bool fixupUmask, const char *commandString);

#endif /* __WORKGROUP_H__ */
