//
// workgroup - UD Cluster utilities, workgroup helper
// config.h
//
// Program-wide includes, etc.
//
// Copyright (c) 2011
// Dr. Jeffrey Frey
// Network & Systems Services, University of Delaware
//
// $Id: config.h 576 2015-10-29 20:02:45Z frey $
//

#ifndef __CONFIG_H__
#define __CONFIG_H__

#define WORKGROUP_VERSION_MAJOR @WORKGROUP_VERSION_MAJOR@
#define WORKGROUP_VERSION_MINOR @WORKGROUP_VERSION_MINOR@
#define WORKGROUP_VERSION_PATCH @WORKGROUP_VERSION_PATCH@
#cmakedefine WORKGROUP_VERSION_STRING "@WORKGROUP_VERSION_STRING@"
#cmakedefine WORKGROUP_ENABLE_SPAWN_CLEAN_ENV

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <unistd.h>
#include <limits.h>
#include <grp.h>
#include <errno.h>

//
// NCurses support:
//
#include <@NCURSES_HEADER@>
#include <menu.h>
#include <form.h>

#cmakedefine HAVE_ASPRINTF @HAVE_ASPRINTF@
#ifndef HAVE_ASPRINTF
int asprintf(char **ret, const char *format, ...);
#endif

#cmakedefine HAVE_STRDUP @HAVE_STRDUP@
#ifndef HAVE_STRDUP
char* strdup(const char* s);
#endif

bool workgroup_is_file(const char *path);
const char* workgroup_path_relative_to_homedir(const char *path);


#cmakedefine HAVE_LIBYAML @HAVE_LIBYAML@
#ifdef HAVE_LIBYAML
#include <yaml.h>
#endif

#cmakedefine WORKGROUP_DEFAULT_MAPFILE "@WORKGROUP_DEFAULT_MAPFILE@"

#cmakedefine WORKGROUP_DIR_PREFIX "@WORKGROUP_DIR_PREFIX@"

#cmakedefine WORKGROUP_GID_LOW @WORKGROUP_GID_LOW@

#cmakedefine ENABLE_USER_MAPFILE @ENABLE_USER_MAPFILE@

#endif /* __CONFIG_H__ */
