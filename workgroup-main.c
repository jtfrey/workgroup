//
// workgroup - UD Cluster utilities, workgroup helper
// workgroup-main.c
//
// Main entry point for the utility.  Also includes the ncurses menu driver
// for interactive selection of a workgroup.
//
// Copyright (c) 2011
// Dr. Jeffrey Frey
// Network & Systems Services, University of Delaware
//
// $Id: workgroup-main.c 575 2015-10-29 19:54:23Z frey $
//

#include "WorkgroupList.h"

//

#include <locale.h>
#include <getopt.h>

static struct option cliOptions[] = {
  { "menu",         no_argument,        NULL,           'm' },
  { "query",        required_argument,  NULL,           'q' },
  { "group",        required_argument,  NULL,           'g' },
  { "cd",           no_argument,        NULL,           'c' },
  { "help",         no_argument,        NULL,           'h' },
  { "version",      no_argument,        NULL,           'V' },
  { "command",      required_argument,  NULL,           'C' },
  { "group-write",  no_argument,        NULL,           'w' },
  { NULL,           0,                  NULL,            0  }
};

static const char *cliOptionsStr = "mq:g:chVC:w";

void
usage(
  char*     exeName
)
{
  printf(
      "\n"
      " Spawn a new shell or run a command as an HPC workgroup.\n\n"
      "  %1$s {options}\n\n"
      " Options:\n\n"
      "  --help/-h                this information\n"
      "  --version/-V             show the program version\n\n"
      "  --query/-q <kind>        display information\n"
      "                           workgroups      show list of available workgroups\n"
      "                           workdirs        show list of work directories\n\n"
      "  --menu/-m                interactive mode; select workgroup from a menu\n"
      "  --group/-g <gid/name>    select the given workgroup\n"
      "  --cd/-c                  also change to the workgroup's work directory\n"
      "  --group-write/-w         update the umask in the workgroup shell to NOT remove\n"
      "                           group-write on newly created directories and files\n"
      "  --command/-C <command>   in the workgroup shell execute the command(s) in\n"
      "                           the provided string\n"
      "                             - multi-word commands must be enclosed in quotes\n"
      "                             - appropriate quoting and escaping is important\n"
      "                             - use a semicolon between multiple commands in the\n"
      "                               string\n"
      "                           if <command> is the single character '@' then all extra\n"
      "                           arguments following a '--' option will form the command:\n"
      "\n"
      "                               ... --command @ -- /usr/bin/env python myscript.py\n"
      "\n"
      "                           would have '/usr/bin/env python myscript.py' as its command\n"
      "                           string\n"
      "\n"
      " Example:\n"
      "\n"
      "    %1$s --cd --group it_nss --command \"grep Gid: /proc/\\$\\$/status\"\n"
      "\n"
#ifdef WORKGROUP_DEFAULT_MAPFILE
      " * Non-static directory mappings read from " WORKGROUP_DEFAULT_MAPFILE "\n\n"
#endif
#ifdef ENABLE_USER_MAPFILE
      " * User directory mapping overrides will be read from ~/.workdirs.yaml\n\n"
#endif
      ,
      exeName
    );
}

//

#define HPAD      4
#define VPAD      2

typedef struct {
  int               width, height;
  WINDOW            *window;
  //
  WorkgroupListRef  workgroupList;
  MENU              *menu;
  WINDOW            *menuWindow;
  //
  FIELD             *formFields[2];
  FORM              *form;
  WINDOW            *formWindow;
  //
  bool              targetIsForm;
  //
  bool              fixupUmask;
  bool              changeToWorkgroupDir;
  const char        *commandString;
} WGMenuWindow;

//

void
WGMenuWindowDrawChangeToWorkgroupDirCheckbox(
  WGMenuWindow*   aWindow
)
{
  mvwprintw(aWindow->window, aWindow->height - 6, 4, "[%c] Also change to group work directory",
      (aWindow->changeToWorkgroupDir ? '*' : ' ')
    );
}
 
//

void
WGMenuWindowDrawWorkDirectoryField(
  WGMenuWindow*   aWindow
)
{
  mvwhline(aWindow->window, aWindow->height - 5, 8, ' ', aWindow->width - 16);
  mvwprintw(aWindow->window, aWindow->height - 5, 8, "$WORKDIR = %s", WorkgroupGetWorkDirectory(WorkgroupListGetSelectedWorkgroupFromMenu(aWindow->workgroupList)));

}

//

WGMenuWindow*
WGMenuWindowInit(
  WorkgroupListRef  aWorkgroupList
)
{
  WGMenuWindow*   aWindow = (WGMenuWindow*)malloc(sizeof(WGMenuWindow));
  
  if ( aWindow ) {
    // ncurses initialization:
    initscr();
    cbreak();
    noecho();
    keypad(stdscr, TRUE);
    curs_set(0);
    
    aWindow->width = COLS - 2 * HPAD;
    aWindow->height = LINES - 2 * VPAD;
    
    aWindow->targetIsForm = FALSE;
    
    aWindow->workgroupList = aWorkgroupList;
    
    // setup a window for the menu stuff:
    aWindow->window = newwin(aWindow->height, aWindow->width, VPAD, HPAD);
    keypad(aWindow->window, TRUE);
    box(aWindow->window, 0, 0);
    
    // menu setup:
    aWindow->menu = WorkgroupListGetCursesMenu(aWorkgroupList);
    aWindow->menuWindow = derwin(aWindow->window, aWindow->height - 12, aWindow->width - 2, 1, 1);
    set_menu_win(aWindow->menu, aWindow->menuWindow);
    set_menu_sub(aWindow->menu, derwin(aWindow->menuWindow, aWindow->height - 14, aWindow->width - 4, 1, 1));
    set_menu_format(aWindow->menu, aWindow->height - 16, 1);
    post_menu(aWindow->menu);
    box(aWindow->menuWindow, 0, 0);
    mvwprintw(aWindow->menuWindow, 0, 2, "[AVAILABLE WORKGROUPS]");
    
    // add the form:
    aWindow->formFields[0] = new_field(1, aWindow->width - 6, 1, 1, 0, 0);
    set_field_back(aWindow->formFields[0], A_UNDERLINE);
    field_opts_on(aWindow->formFields[0], O_ACTIVE);
    field_opts_on(aWindow->formFields[0], O_EDIT);
    field_opts_off(aWindow->formFields[0], O_STATIC);
    field_opts_off(aWindow->formFields[0], O_BLANK);
    aWindow->formFields[1] = NULL;
    aWindow->form = new_form(aWindow->formFields);
    aWindow->formWindow = derwin(aWindow->window, 5, aWindow->width - 2, aWindow->height - 11, 1);
    set_form_win(aWindow->form, aWindow->formWindow);
    set_form_sub(aWindow->form, derwin(aWindow->formWindow, 3, aWindow->width - 4, 1, 1));
    post_form(aWindow->form);
    box(aWindow->formWindow, 0, 0);
    mvwprintw(aWindow->formWindow, 0, 2, " COMMAND TO EXECUTE ");
    
    mvwaddch(aWindow->window, aWindow->height - 4, 0, ACS_LTEE);
    mvwhline(aWindow->window, aWindow->height - 4, 1, ACS_HLINE, aWindow->width - 2);
    mvwaddch(aWindow->window, aWindow->height - 4, aWindow->width - 1, ACS_RTEE);
    mvwprintw(aWindow->window, aWindow->height - 3, 2, "Commands:  [S]pawn workgroup shell  [C]hange to work directory  [Q]uit (also <ESC> or <F1>)");
    mvwprintw(aWindow->window, aWindow->height - 2, 13, "use <TAB> to move between the menu and command areas");
    
    // initial checkbox setup:
    aWindow->changeToWorkgroupDir = false;
    WGMenuWindowDrawChangeToWorkgroupDirCheckbox(aWindow);
    
    // display selected work directory:
    WGMenuWindowDrawWorkDirectoryField(aWindow);
    
    refresh();
    wrefresh(aWindow->formWindow);
    wrefresh(aWindow->menuWindow);
    wrefresh(aWindow->window);
  }
  return aWindow;
}

//

int
WGMenuWindowRunloop(
  WGMenuWindow*   aWindow
)
{
  int       done = 0;
  
  set_field_buffer(aWindow->formFields[0], 0, (aWindow->commandString ? aWindow->commandString : ""));
  refresh();
  wrefresh(aWindow->menuWindow);
  wrefresh(aWindow->formWindow);
  wrefresh(aWindow->window);
  
  while( ! done ) {
    int     c = getch();
    
    switch (c) {
    
      case 27:
      case KEY_F(1):
        done = 1;
        break;
    
        
      case KEY_ENTER:
      case '\n':
      case '\r':
        if ( ! aWindow->targetIsForm ) break;
      case '\t':
        if ( (aWindow->targetIsForm = ! aWindow->targetIsForm) ) {
          form_driver(aWindow->form, REQ_END_LINE);
        } else {
          form_driver(aWindow->form, REQ_BEG_LINE);
        }
        mvwprintw(aWindow->menuWindow, 0, 2, ( aWindow->targetIsForm ? " AVAILABLE WORKGROUPS " : "[AVAILABLE WORKGROUPS]"));
        mvwprintw(aWindow->formWindow, 0, 2, ( aWindow->targetIsForm ? "[COMMAND TO EXECUTE]" : " COMMAND TO EXECUTE "));
        break;
        
      case KEY_DOWN:
        if ( aWindow->targetIsForm ) {
          form_driver(aWindow->form, REQ_NEXT_FIELD);
          form_driver(aWindow->form, REQ_END_LINE);
        } else {
          menu_driver(aWindow->menu, REQ_DOWN_ITEM);
          // display selected work directory:
          WGMenuWindowDrawWorkDirectoryField(aWindow);
        }
        break;
      case KEY_UP:
        if ( aWindow->targetIsForm ) {
          form_driver(aWindow->form, REQ_PREV_FIELD);
          form_driver(aWindow->form, REQ_END_LINE);
        } else {
          menu_driver(aWindow->menu, REQ_UP_ITEM);
          // display selected work directory:
          WGMenuWindowDrawWorkDirectoryField(aWindow);
        }
        break;
        
      case KEY_LEFT:
        if ( aWindow->targetIsForm ) form_driver(aWindow->form, REQ_LEFT_CHAR);
        break;
      case KEY_RIGHT:
        if ( aWindow->targetIsForm ) form_driver(aWindow->form, REQ_RIGHT_CHAR);
        break;
      case KEY_DL:
        if ( aWindow->targetIsForm ) form_driver(aWindow->form, REQ_CLR_FIELD);
        break;
      case 0x7f:
      case KEY_BACKSPACE:
        if ( aWindow->targetIsForm ) {
          form_driver(aWindow->form, REQ_LEFT_CHAR);
          form_driver(aWindow->form, REQ_DEL_CHAR);
        }
        break;
      
      case 'S':
      case 's': {
        if ( ! aWindow->targetIsForm ) {
          int         rc;
          const char  *commandString;
          
          form_driver(aWindow->form, REQ_VALIDATION);
          if ( (commandString = field_buffer(aWindow->formFields[0], 0)) ) {
            while ( *commandString ) {
              if ( ! isspace(*commandString) ) break;
              commandString++;
            }
            if ( *commandString == '\0' ) commandString = NULL; 
          }
          erase();
          refresh();
          endwin();
          rc = ( commandString
                  ? WorkgroupExecCommand(WorkgroupListGetSelectedWorkgroupFromMenu(aWindow->workgroupList), aWindow->changeToWorkgroupDir, aWindow->fixupUmask, commandString)
                  : WorkgroupSpawnSubshell(WorkgroupListGetSelectedWorkgroupFromMenu(aWindow->workgroupList), aWindow->changeToWorkgroupDir, aWindow->fixupUmask)
                );
          printf("\nERROR:  Could not change to the selected workgroup (code = %d)\n\n", rc);
          fflush(stdout);
          exit(EINVAL);
        }
      }
      
      case 'Q':
      case 'q':
        if ( ! aWindow->targetIsForm ) {
          done = 1;
          break;
        }
        
      case 'C':
      case 'c':
        if ( ! aWindow->targetIsForm ) {
          aWindow->changeToWorkgroupDir = ! aWindow->changeToWorkgroupDir;
          WGMenuWindowDrawChangeToWorkgroupDirCheckbox(aWindow);
          break;
        }
      
      default:
        if ( aWindow->targetIsForm ) form_driver(aWindow->form, c );
        break;
      
    }
    
    refresh();
    wrefresh(aWindow->formWindow);
    wrefresh(aWindow->menuWindow);
    wrefresh(aWindow->window);
  }
  unpost_form(aWindow->form);
  free_field(aWindow->formFields[0]);
	free_form(aWindow->form);
  delwin(aWindow->formWindow);
  
  unpost_menu(aWindow->menu);
  delwin(aWindow->menuWindow);
  // The menu will be destroyed when the WorkgroupList object
  // is destroyed, so don't do it now.
  
  delwin(aWindow->window);
  
  erase();
  refresh();
  endwin();
  
  return 0;
}

//

int
doQuery(
  WorkgroupListRef    wgList,
  const char*         targetGid,
  const char*         queryKind
)
{
  WorkgroupRef        targetWG = ( targetGid ? WorkgroupListGetWorkgroupByCString(wgList, targetGid) : NULL );
  
  if ( strcmp(queryKind, "workgroups") == 0 ) {
    unsigned int  i = 0, iMax = WorkgroupListGetCount(wgList);
    
    while ( i < iMax ) {
      WorkgroupRef  wg = WorkgroupListGetWorkgroupAtIndex(wgList, i++);
      
      printf(" %-5d %s\n", WorkgroupGetGidNumber(wg), WorkgroupGetGidString(wg));
    }
  }
  else if ( strcmp(queryKind, "workdirs") == 0 ) {
    if ( targetGid ) {
      printf("%s\n", WorkgroupGetWorkDirectory(targetWG));
    } else {
      unsigned int  i = 0, iMax = WorkgroupListGetCount(wgList);
      
      while ( i < iMax ) {
        WorkgroupRef  wg = WorkgroupListGetWorkgroupAtIndex(wgList, i++);
        
        printf(" %-5d %s\n", WorkgroupGetGidNumber(wg), WorkgroupGetWorkDirectory(wg));
      }
    }
  }
  else {
    printf("ERROR:  Invalid query kind:  %s\n\n", queryKind);
    return EINVAL;
  }
  return 0;
}

//

int
main(
  int       argc,
  char*     argv[]
)
{
  int                   rc = 0;
  WorkgroupListRef      wgList = WorkgroupListCreate();
  char*                 exeName = argv[0];

  setlocale(LC_ALL, "");
  
  //
  // So long as a WorkgroupList is returned, the user is a member of at least one
  // workgroup:
  //
  if ( wgList ) {
    //
    // Check for CLI arguments:
    //
    char                whichOption;
    bool                doMenu = false;
    bool                doUmaskFixup = false;
    char*               gidStr = NULL;
    bool                doChangeWorkingDirectory = false;
    const char          *commandString = NULL;
    
    if ( argc < 2 ) {
      usage(exeName);
      exit(EINVAL);
    }
    
    while ( (whichOption = getopt_long(argc, argv, cliOptionsStr, cliOptions, NULL)) != -1) {
      switch (whichOption) {
      
        case 'm': {
          doMenu = true;
          break;
        }
        
        case 'g': {
          gidStr = optarg;
          break;
        }
        
        case 'c': {
          doChangeWorkingDirectory = true;
          break;
        }
        
        case 'q':
          return doQuery(wgList, gidStr, optarg);
        
        case 'h': {
          usage(exeName);
          exit(0);
        }

        case 'V': {
          printf(
              "%s\n"
              "(built %s %s)\n",
              WORKGROUP_VERSION_STRING,
              __DATE__, __TIME__
            );
          exit(0);
        }
        
        case 'C': {
          if ( optarg && *optarg ) commandString = optarg;
          break;
        }
        
        case 'w': {
          doUmaskFixup = true;
          break;
        }
        
      }
    }
    // Skip-over all the CLI stuff that getopt processed:
    argc -= optind;
    argv += optind;
    
    // Was the <command> provided == '@'?
    if ( commandString && (strcmp(commandString, "@") == 0) ) {
        size_t      commandStringLen = 0;
        char        *commandStringPtr;
        int         argn;
        
        // Are there any arguments left?
        if ( argc <= 0 ) {
            printf("ERROR:  No additional arguments left to form command string (--command/-C was '@')\n");
            exit(rc);
        }
        // Get total length or commandString we will synthesize:
        argn = 0;
        while ( argn < argc ) commandStringLen += strlen(argv[argn++]) + 1;
        if ( commandStringLen <= 0 ) {
            printf("ERROR:  No additional arguments left to form command string (--command/-C was '@')\n");
            exit(rc);
        }
        // Allocate the string:
        commandStringPtr = (char*)malloc(commandStringLen);
        if ( ! commandStringPtr ) {
            printf("ERROR:  Unable to allocate sythesized command string (errno = %d)\n", errno);
            exit(errno);
        }
        commandString = commandStringPtr;
        argn = 0;
        while ( argn < argc ) {
            int     nchar = snprintf(commandStringPtr, commandStringLen, "%s", argv[argn++]);
            
            if ( nchar < 0 ) {
                printf("ERROR:  Failure while synthesizing command string (errno = %d)\n", errno);
                exit(errno);
            }
            commandStringPtr += nchar;
            commandStringLen -= nchar;
            if ( argn < argc ) {
                *commandStringPtr++ = ' ';
                commandStringLen--;
            }
        }
        *commandStringPtr = '\0';
    }
    
    if ( gidStr ) {
      WorkgroupRef    wg = WorkgroupListGetWorkgroupByCString(wgList, gidStr);
      
      if ( wg ) {
        //
        // All was okay, try to spawn a shell or run the command now:
        //
        rc = ( commandString ? WorkgroupExecCommand(wg, doChangeWorkingDirectory, doUmaskFixup, commandString) : WorkgroupSpawnSubshell(wg, doChangeWorkingDirectory, doUmaskFixup) );
      } else {
        printf("ERROR:  You are not a member of workgroup '%s'\n", gidStr);
        rc = EINVAL;
      }
    }
    else if ( doMenu ) {
      //
      // Do the interactive menu:
      //
      WGMenuWindow*   wgTUI = WGMenuWindowInit(wgList);
      
      wgTUI->commandString = commandString;
      wgTUI->fixupUmask = doUmaskFixup;
      if ( doChangeWorkingDirectory ) {
        wgTUI->changeToWorkgroupDir = true;
        WGMenuWindowDrawChangeToWorkgroupDirCheckbox(wgTUI);
      }
      rc = WGMenuWindowRunloop(wgTUI);
    }
    else {
      //
      // Show usage and exit now if the user gave no group and didn't request interactive
      // selection:
      //
      usage(exeName);
      rc = EINVAL;
    }
    WorkgroupListFree(wgList);
  } else {
    fprintf(stderr, "You are not a member of any workgroups.\n");
    rc = EACCES;
  }
  return rc;
}
