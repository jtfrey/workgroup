//
// workgroup - UD Cluster utilities, workgroup helper
// WorkgroupList.h
//
// Public interfaces to the WorkgroupList pseudo-class.
//
// Copyright (c) 2011
// Dr. Jeffrey Frey
// Network & Systems Services, University of Delaware
//
// $Id: WorkgroupList.h 372 2011-11-28 20:00:05Z frey $
//

#ifndef __WORKGROUPLIST_H__
#define __WORKGROUPLIST_H__

#include "Workgroup.h"

typedef struct _WorkgroupList* WorkgroupListRef;

WorkgroupListRef WorkgroupListCreate();
void WorkgroupListFree(WorkgroupListRef aWorkgroupList);

unsigned int WorkgroupListGetCount(WorkgroupListRef aWorkgroupList);
WorkgroupRef WorkgroupListGetWorkgroupAtIndex(WorkgroupListRef aWorkgroupList, unsigned int index);

MENU* WorkgroupListGetCursesMenu(WorkgroupListRef aWorkgroupList);
WorkgroupRef WorkgroupListGetSelectedWorkgroupFromMenu(WorkgroupListRef aWorkgroupList);

WorkgroupRef WorkgroupListGetWorkgroupByCString(WorkgroupListRef aWorkgroupList, const char* gidString);

WorkgroupRef WorkgroupListGetWorkgroupByGidNumber(WorkgroupListRef aWorkgroupList, gid_t gidNumber);
WorkgroupRef WorkgroupListGetWorkgroupByName(WorkgroupListRef aWorkgroupList, const char* gidName);


#endif /* __WORKGROUPLIST_H__ */
