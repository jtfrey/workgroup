//
// workgroup - UD Cluster utilities, workgroup helper
// WorkdirMap.c
//
// Implementation of the Workgroup directory mapping
// functionality.
//
// Copyright (c) 2011
// Dr. Jeffrey Frey
// Network & Systems Services, University of Delaware
//

#include "WorkdirMap.h"

//

#ifndef WORKGROUP_DIR_PREFIX
#define WORKGROUP_DIR_PREFIX NULL
#endif

#ifndef WORKGROUP_DEFAULT_MAPFILE
#define WORKGROUP_DEFAULT_MAPFILE NULL
#endif

//

typedef void (*WorkdirMapDeallocCallback)(WorkdirMapRef aMap);
typedef const char* (*WorkdirMapDirectoryForGroup)(WorkdirMapRef aMap, gid_t gidNumber, const char *gidName);

typedef struct __WorkdirMap {
  unsigned int                refCount;
  
  WorkdirMapDeallocCallback   deallocCallback;
  WorkdirMapDirectoryForGroup directoryForGroupCallback;
} WorkdirMap;

//

WorkdirMap*
_WorkdirMapAlloc(
  size_t                      extraBytes,
  WorkdirMapDeallocCallback   deallocCallback,
  WorkdirMapDirectoryForGroup directoryForGroupCallback
)
{
  WorkdirMap  *newMap = malloc(sizeof(WorkdirMap) + extraBytes);
  
  if ( newMap ) {
    newMap->refCount = 1;
    newMap->deallocCallback = deallocCallback;
    newMap->directoryForGroupCallback = directoryForGroupCallback;
  }
  return newMap;
}

//

void
_WorkdirMapDealloc(
  WorkdirMap    *aMap
)
{
  if ( aMap->deallocCallback ) aMap->deallocCallback((WorkdirMapRef)aMap);
  free((void*)aMap);
}

//

WorkdirMapRef
WorkdirMapRetain(
  WorkdirMapRef   aMap
)
{
  aMap->refCount++;
  return aMap;
}

//

void
WorkdirMapRelease(
  WorkdirMapRef   aMap
)
{
  if ( --aMap->refCount == 0 ) _WorkdirMapDealloc((WorkdirMap*)aMap);
}

//
#if 0
#pragma mark -
#endif
//

typedef struct __WorkdirMapStatic {
  WorkdirMap        base;
  
  const char        *templateFormat;
} WorkdirMapStatic;

//

const char*
_WorkdirMapStaticDirectoryForGroup(
  WorkdirMapRef   aMap,
  gid_t           gidNumber,
  const char      *gidName
)
{
  WorkdirMapStatic  *MAP = (WorkdirMapStatic*)aMap;
  char              *workDir = NULL;
  
  if ( asprintf((char**)&workDir, MAP->templateFormat, gidName) < 0 )
    workDir = NULL;
  return (const char*)workDir;
}

//

WorkdirMapRef
WorkdirMapCreateStatic(
  const char    *basePath
)
{
  WorkdirMapStatic  *newMap;
  size_t            basePathLen = strlen(basePath);
  
  /* Add the /%s and NUL onto the base path: */
  basePathLen += 4;
  
  /* Allocate the object: */
  newMap = (WorkdirMapStatic*)_WorkdirMapAlloc
                (sizeof(WorkdirMapStatic) - sizeof(WorkdirMap) + basePathLen,
                NULL,
                _WorkdirMapStaticDirectoryForGroup
              );
  if ( newMap ) {
    newMap->templateFormat = ((void*)newMap) + sizeof(WorkdirMapStatic);
    snprintf((char*)newMap->templateFormat, basePathLen, "%s/%%s", basePath);
  }
  return (WorkdirMapRef)newMap;
}

//
#if 0
#pragma mark -
#endif
#ifdef HAVE_LIBYAML
//

typedef struct __WorkdirMapYAML {
  WorkdirMap        base;
  
  const char        *yamlFile;
  bool              yamlDocIsInited;
  yaml_document_t   yamlDoc;
} WorkdirMapYAML;

//

void
_WorkdirMapYAMLDeallocCallback(
  WorkdirMapRef   aMap
)
{
  WorkdirMapYAML  *MAP = (WorkdirMapYAML*)aMap;
  
  if ( MAP->yamlDocIsInited ) yaml_document_delete(&MAP->yamlDoc);
}

//

const char*
_WorkdirMapYAMLDirectoryForGroup(
  WorkdirMapRef   aMap,
  gid_t           gidNumber,
  const char      *gidName
)
{
  WorkdirMapYAML    *MAP = (WorkdirMapYAML*)aMap;
  char              *workDir = NULL;
  yaml_node_t       *rootNode = yaml_document_get_root_node(&MAP->yamlDoc);
    
  if ( rootNode && (rootNode->type == YAML_MAPPING_NODE) ) {
    yaml_node_pair_t    *s = rootNode->data.mapping.pairs.start;
    yaml_node_pair_t    *e = rootNode->data.mapping.pairs.top;
    char                gidNumberStr[12];
    
    /* Stash the gidNumber in string form for easy comparison: */
    snprintf(gidNumberStr, sizeof(gidNumberStr), "%d", (int)gidNumber);
    
    /* Walk the mappings: */
    while ( s < e ) {
      if ( s->key && s->value ) {
        yaml_node_t       *key = yaml_document_get_node(&MAP->yamlDoc, s->key);
        yaml_node_t       *value = yaml_document_get_node(&MAP->yamlDoc, s->value);
        
        if ( strcmp(key->data.scalar.value, gidNumberStr) == 0 ) {
          workDir = strndup(value->data.scalar.value, value->data.scalar.length);
          break;
        }
      }
      s++;
    }
    
    /* If we haven't found numeric gid, try by name: */
    if ( ! workDir ) {
      s = rootNode->data.mapping.pairs.start;
      e = rootNode->data.mapping.pairs.top;
      
      /* Walk the mappings: */
      while ( s < e ) {
        if ( s->key && s->value ) {
          yaml_node_t       *key = yaml_document_get_node(&MAP->yamlDoc, s->key);
          yaml_node_t       *value = yaml_document_get_node(&MAP->yamlDoc, s->value);
          
          if ( strcmp(key->data.scalar.value, gidName) == 0 ) {
            workDir = strndup(value->data.scalar.value, value->data.scalar.length);
            break;
          }
        }
        s++;
      }
    }
  }
  return (const char*)workDir;
}

//

WorkdirMapRef
WorkdirMapCreateYAML(
  const char    *yamlFile
)
{
  WorkdirMapYAML    *newMap;
  size_t            yamlFileLen = strlen(yamlFile) + 1;
  
  // Allocate the new instance:
  newMap = (WorkdirMapYAML*)_WorkdirMapAlloc(
                    sizeof(WorkdirMapYAML) - sizeof(WorkdirMap) + yamlFileLen,
                    _WorkdirMapYAMLDeallocCallback,
                    _WorkdirMapYAMLDirectoryForGroup
                  );
  if ( newMap ) {
    FILE            *inFPtr;
    bool            okay = false;
    
    newMap->yamlFile = ((void*)newMap) + sizeof(WorkdirMapYAML);
    strncpy((char*)newMap->yamlFile, yamlFile, yamlFileLen);
    
    newMap->yamlDocIsInited = false;
    
    // Open the map: */
    inFPtr = fopen(yamlFile, "rb");
    if ( inFPtr ) {
      yaml_parser_t parser;
      
      // Create the Parser object:
      yaml_parser_initialize(&parser);

      // Set the parser to walk the map file:
      yaml_parser_set_input_file(&parser, inFPtr);
      
      // Load the document:
      if ( yaml_parser_load(&parser, &newMap->yamlDoc) ) {
        yaml_node_t *rootNode = yaml_document_get_root_node(&newMap->yamlDoc);
        
        newMap->yamlDocIsInited = true;
        if ( rootNode && (rootNode->type == YAML_MAPPING_NODE) ) {
          okay = true;
        } else {
          if ( rootNode ) {
            fprintf(stderr, "ERROR:  mapping file `%s` structure is wrong (root node is not a mapping)\n", yamlFile);
          } else {
            // Empty document is okay:
            okay = true;
          }
        }
      } else {
        fprintf(stderr, "ERROR:  mapping file `%s` could not be parsed\n", yamlFile);
      }
        
      /* Dispose of the parser: */
      yaml_parser_delete(&parser);

      /* Close the mapping file: */
      fclose(inFPtr);
    }
    
    // Get rid of the object if we failed:
    if ( ! okay ) {
      WorkdirMapRelease((WorkdirMapRef)newMap);
      newMap = NULL;
    }
  }
  return (WorkdirMapRef)newMap;
}

//
#endif
#if 0
#pragma mark -
#endif
//

typedef struct __WorkdirMapStackNode {
  struct __WorkdirMapStackNode    *link;
  
  WorkdirMapRef                   mapper;
} WorkdirMapStackNode;

//

WorkdirMapStackNode*
_WorkdirMapStackNodeAlloc(
  WorkdirMapRef     mapper
)
{
  WorkdirMapStackNode   *newNode = malloc(sizeof(WorkdirMapStackNode));
  
  if ( newNode ) {
    newNode->link = NULL;
    newNode->mapper = WorkdirMapRetain(mapper);
  }
  return newNode;
}

//

void
_WorkdirMapStackNodeDealloc(
  WorkdirMapStackNode   *theNode
)
{
  if ( theNode->mapper ) WorkdirMapRelease(theNode->mapper);
  free((void*)theNode);
}

//

typedef struct __WorkdirMapStack {
  unsigned int          refCount;
  
  WorkdirMapStackNode   *stack;
} WorkdirMapStack;

//

WorkdirMapStackRef
WorkdirMapStackGetDefault()
{
  static WorkdirMapStackRef   defaultMapStack = NULL;
  static char                 *defaultBasePath = WORKGROUP_DIR_PREFIX;
  static char                 *defaultMapFile = WORKGROUP_DEFAULT_MAPFILE;
  
  if ( ! defaultMapStack ) {
    defaultMapStack = WorkdirMapStackCreate();
    if ( defaultMapStack && defaultBasePath ) {
      WorkdirMapRef           baseMap = WorkdirMapCreateStatic(defaultBasePath);
      
      if ( baseMap ) {
        WorkdirMapStackPush(defaultMapStack, baseMap);
        WorkdirMapRelease(baseMap);
      }
    }
#ifdef HAVE_LIBYAML
    if ( defaultMapStack && defaultMapFile && workgroup_is_file(defaultMapFile) ) {
      WorkdirMapRef           yamlMap = WorkdirMapCreateYAML(defaultMapFile);
      
      if ( yamlMap ) {
        WorkdirMapStackPush(defaultMapStack, yamlMap);
        WorkdirMapRelease(yamlMap);
      }
    }
    
# ifdef ENABLE_USER_MAPFILE

    if ( defaultMapStack ) {
      const char      *userMapFile = workgroup_path_relative_to_homedir(".workdirs.yaml");
      
      if ( userMapFile ) {
        if ( workgroup_is_file(userMapFile) ) {
          WorkdirMapRef         yamlMap = WorkdirMapCreateYAML(userMapFile);
      
          if ( yamlMap ) {
            WorkdirMapStackPush(defaultMapStack, yamlMap);
            WorkdirMapRelease(yamlMap);
          }
        }
        free((void*)userMapFile);
      }
      
    }

# endif
#endif
  }
  return defaultMapStack;
}

//

WorkdirMapStackRef
WorkdirMapStackCreate()
{
  WorkdirMapStack   *newStack = malloc(sizeof(WorkdirMapStack));
  
  if ( newStack ) {
    newStack->refCount = 0;
    newStack->stack = NULL;
  }
  return newStack;
}

//

WorkdirMapStackRef
WorkdirMapStackRetain(
  WorkdirMapStackRef  aMapStack
)
{
  aMapStack->refCount++;
  return aMapStack;
}

//

void
WorkdirMapStackRelease(
  WorkdirMapStackRef  aMapStack
)
{
  if ( --aMapStack->refCount == 0 ) {
    WorkdirMapStackNode   *node = aMapStack->stack;
    
    while ( node ) {
      WorkdirMapStackNode *next = node->link;
      
      _WorkdirMapStackNodeDealloc(node);
      node = next;
    }
    free((void*)aMapStack);
  }
}

//

bool
WorkdirMapStackPush(
  WorkdirMapStackRef  aMapStack,
  WorkdirMapRef       aMap
)
{
  WorkdirMapStackNode *node = aMapStack->stack;
  
  while ( node ) {
    if ( node->mapper == aMap ) return true;
    node = node->link;
  }
  
  // Not already on the stack:
  node = _WorkdirMapStackNodeAlloc(aMap);
  if ( node ) {
    node->link = aMapStack->stack;
    aMapStack->stack = node;
  }
  return false;
}

//

const char*
WorkdirMapStackDirectoryForGroup(
  WorkdirMapStackRef  aMapStack,
  gid_t               gidNumber,
  const char          *gidName
)
{
  WorkdirMapStackNode *node = aMapStack->stack;
  const char          *workDir = NULL;
  
  while ( ! workDir && node ) {
    workDir = node->mapper->directoryForGroupCallback(node->mapper, gidNumber, gidName);
    node = node->link;
  }
  return workDir;
}
