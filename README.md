# workgroup

Command similar to `newgrp` that includes UD cluster-specific functionality:

- Spawn new shell (or command) inside the specified workgroup's work directory

- Command-line mode and TUI menu-based mode

Various command-line flags are supported:

```
$ ./workgroup --help

 Spawn a new shell or run a command as an HPC workgroup.

  ./workgroup {options}

 Options:

  --help/-h                this information
  --version/-V             show the program version

  --query/-q <kind>        display information
                           workgroups      show list of available workgroups
                           workdirs        show list of work directories

  --menu/-m                interactive mode; select workgroup from a menu
  --group/-g <gid/name>    select the given workgroup
  --cd/-c                  also change to the workgroup's work directory
  --command/-C <command>   in the workgroup shell execute the command(s) in
                           the provided string
                             - multi-word commands must be enclosed in quotes
                             - appropriate quoting and escaping is important
                             - use a semicolon between multiple commands in the
                               string

 Example:

    ./workgroup --cd --group it_nss --command "grep Gid: /proc/\$\$/status"

 * Non-static directory mappings read from /opt/shared/workgroup/etc/workdirs.yaml

 * User directory mapping overrides will be read from ~/.workdirs.yaml
```

## Work directory maps

Note in the help screen cited above the mention of *directory mappings*.  By default, the `workgroup` and `workdir` commands append the Unix group name to a base path when determining the `$WORKDIR` for a workgroup.  Directory mappings selectively override that behavior:  the Unix group name is looked-up in the mapping file.  For example, the user could create a `~/.workdirs.yaml` file:

```
# Override these workgroup work directories for me:
sysadmin:    /opt/shared/sysadmin
slurmadm:    /opt/shared/slurm/current
```

If the base workgroup directory is `/work`, then the above data is reflected in the output of `workdir`:

```
$ workdir -g sysadmin
/opt/shared/sysadmin
```

Were I to remove the `sysadmin` key-value pair from that file:

```
$ workdir -g sysadmin
/work/sysadmin
```

The overrides are probably most useful for users who want to start-out inside their own directory in workgroup storage:

```
$ ln -sf /work/workgroup1/sw /work/workgroup1/users/myname/sw
$ cat ~/.workdirs.yaml
workgroup1:   /work/workgroup1/users/myname
```

Symlink'ing the workgroup `sw` directory into the user's directory allows VALET to still find the workgroup package definitions at `$WORKDIR/sw/valet`.
