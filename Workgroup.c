//
// workgroup - UD Cluster utilities, workgroup helper
// Workgroup.c
//
// Implementation of the Workgroup pseudo-class.
//
// Copyright (c) 2011
// Dr. Jeffrey Frey
// Network & Systems Services, University of Delaware
//
// $Id: Workgroup.c 576 2015-10-29 20:02:45Z frey $
//

#include "Workgroup.h"
#include "WorkdirMap.h"

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

const char* WorkgroupWorkDirEnv = "WORKDIR";
const char* WorkgroupWorkDirSwEnv = "WORKDIR_SW";
const char* WorkgroupWorkDirUserEnv = "WORKDIR_USER";
const char* WorkgroupWorkgroupEnv = "WORKGROUP";
const char* WorkgroupFixupUmaskEnv = "WORKGROUP_FIXUP_GROUP_PERMS";

void
__WorkgroupFixupUmask(void)
{
  mode_t        old_mask = umask(S_IRWXG);

  // Remove the group bits if they're set:
  old_mask &= ~S_IRWXG;
  umask(old_mask);
}

//

static const char* WorkgroupMandatoryEnvVarList[] = {
        "DISPLAY",
        "HISTCONTROL",
        "HISTSIZE",
        "HOME",
        "HOSTNAME",
        "LANG",
        "LANGUAGE",
        "LC_ADDRESS",
        "LC_ALL",
        "LC_COLLATE",
        "LC_CTYPE",
        "LC_IDENTIFICATION",
        "LC_MEASUREMENT",
        "LC_MESSAGES",
        "LC_MONETARY",
        "LC_NAME",
        "LC_NUMERIC",
        "LC_PAPER",
        "LC_TELEPHONE",
        "LC_TIME",
        "LOGNAME",
        "MAIL",
        "SHELL",
        "SSH_CLIENT",
        "SSH_CONNECTION",
        "SSH_TTY",
        "TERM",
        "USER",
        "XDG_RUNTIME_DIR",
        "XDG_SESSION_ID"
    };

static int __WorkgroupMandatoryEnvVarListComparLength = 0;
int __WorkgroupMandatoryEnvVarListCompar(
  const void    *a,
  const void    *b
)
{
  return strncmp((char*)a, *(char**)b, __WorkgroupMandatoryEnvVarListComparLength);
}

bool
__WorkgroupMandatoryEnvVar(
  const char  *envvar,
  int         envvarLen
)
{
  if (envvarLen < 0) {
    const char  *p = envvar;
        
    envvarLen = 0;
    while ( *p && (*p != '=') ) p++, envvarLen++;
  }
  if ( envvarLen > 0 ) {
    __WorkgroupMandatoryEnvVarListComparLength = envvarLen;
    return (bsearch(envvar, WorkgroupMandatoryEnvVarList, sizeof(WorkgroupMandatoryEnvVarList) / sizeof(const char*), sizeof(const char*), __WorkgroupMandatoryEnvVarListCompar) != NULL) ? true : false;
  }
  return false;
}

//

char**
__WorkgroupCreateCleanEnvironment(
  const char*   gid,
  const char*   workDirectory
)
{
  extern char** environ;
  char**        newEnviron = NULL;
  char**        ptr = environ;
  char          *workDirSw = NULL, *workDirUser = NULL;
  struct stat	finfo;
  int           count = 0;
  size_t        bytes = 0;

  // Count the pairs:
  while ( ptr && *ptr ) {
    // One we need to keep?
    if ( __WorkgroupMandatoryEnvVar(*ptr, -1) ) {
      bytes += 1 + strlen(*ptr);
      count++;
    }
    ptr++;
  }
  if ( gid && *gid ) { bytes += 1 + strlen(gid) + 1 + strlen(WorkgroupWorkgroupEnv); count++; }
  if ( workDirectory && *workDirectory ) {
    bytes += 1 + strlen(workDirectory) + 1 + strlen(WorkgroupWorkDirEnv);
    count++;

    // Check for WORKDIR_SW extant:
    if ( asprintf(&workDirSw, "%s/sw", workDirectory) > 0 ) {
      if ( (stat(workDirSw, &finfo) == 0) && S_ISDIR(finfo.st_mode) ) {
        bytes += 1 + strlen(workDirSw) + 1 + strlen(WorkgroupWorkDirSwEnv);
        count++;
      } else {
        free((void*)workDirSw);
        workDirSw = NULL;
      }
    }

    // Check for WORKDIR_USER extant:
    if ( asprintf(&workDirUser, "%s/users/%d", workDirectory, (int)getuid()) > 0 ) {
      if ( (stat(workDirUser, &finfo) == 0) && S_ISDIR(finfo.st_mode) ) {
        bytes += 1 + strlen(workDirUser) + 1 + strlen(WorkgroupWorkDirUserEnv);
        count++;
      } else {
        free((void*)workDirUser);
        workDirUser = NULL;
      }
    }
  }
  if ( (newEnviron = malloc((1 + count) * sizeof(char*) + bytes)) ) {
    char*       basePtr = (void*)newEnviron + (1 + count) * sizeof(char*);
    char**      arrayPtr = newEnviron;

    ptr = environ;
    while ( ptr && *ptr ) {
      // One we need to keep?
      if ( __WorkgroupMandatoryEnvVar(*ptr, -1) ) {
        bytes = 1 + strlen(*ptr);
        strncpy((*arrayPtr = basePtr), *ptr, bytes);
        arrayPtr++;
        basePtr += bytes;
      }
      ptr++;
    }
    if ( gid && *gid ) {
      basePtr += 1 + sprintf((*arrayPtr = basePtr), "%s=%s", WorkgroupWorkgroupEnv, gid);
      arrayPtr++;
    }
    if ( workDirectory && *workDirectory ) {
      basePtr += 1 + sprintf((*arrayPtr = basePtr), "%s=%s", WorkgroupWorkDirEnv, workDirectory);
      arrayPtr++;

      if ( workDirSw ) {
        basePtr += 1 + sprintf((*arrayPtr = basePtr), "%s=%s", WorkgroupWorkDirSwEnv, workDirSw);
        arrayPtr++;
        free(workDirSw);
      }
      if ( workDirUser ) {
        basePtr += 1 + sprintf((*arrayPtr = basePtr), "%s=%s", WorkgroupWorkDirUserEnv, workDirUser);
        arrayPtr++;
        free(workDirUser);
      }
    }
    *arrayPtr = NULL;
  }
  return newEnviron;
}

//

char**
__WorkgroupCreateEnvironment(
  const char*   gid,
  const char*   workDirectory
)
{
  extern char** environ;
  char**        newEnviron = NULL;
  char**        ptr = environ;
  char          *workDirSw = NULL, *workDirUser = NULL;
  struct stat   finfo;
  int           count = 0;
  size_t        bytes = 0;

  // Count the pairs:
  while ( ptr && *ptr ) {
    bytes += 1 + strlen(*ptr);
    ptr++;
    count++;
  }
  if ( gid && *gid ) { bytes += 1 + strlen(gid) + 1 + strlen(WorkgroupWorkgroupEnv); count++; }
  if ( workDirectory && *workDirectory ) {
    bytes += 1 + strlen(workDirectory) + 1 + strlen(WorkgroupWorkDirEnv);
    count++;

    // Check for WORKDIR_SW extant:
    if ( asprintf(&workDirSw, "%s/sw", workDirectory) > 0 ) {
      if ( (stat(workDirSw, &finfo) == 0) && S_ISDIR(finfo.st_mode) ) {
        bytes += 1 + strlen(workDirSw) + 1 + strlen(WorkgroupWorkDirSwEnv);
        count++;
      } else {
        free((void*)workDirSw);
        workDirSw = NULL;
      }
    }

    // Check for WORKDIR_USER extant:
    if ( asprintf(&workDirUser, "%s/users/%d", workDirectory, (int)getuid()) > 0 ) {
      if ( (stat(workDirUser, &finfo) == 0) && S_ISDIR(finfo.st_mode) ) {
        bytes += 1 + strlen(workDirUser) + 1 + strlen(WorkgroupWorkDirUserEnv);
        count++;
      } else {
        free((void*)workDirUser);
        workDirUser = NULL;
      }
    }
  }

  if ( (newEnviron = malloc((1 + count) * sizeof(char*) + bytes)) ) {
    char*       basePtr = (void*)newEnviron + (1 + count) * sizeof(char*);
    char**      arrayPtr = newEnviron;

    ptr = environ;
    while ( ptr && *ptr ) {
      bytes = 1 + strlen(*ptr);
      strncpy((*arrayPtr = basePtr), *ptr, bytes);
      arrayPtr++;
      basePtr += bytes;
      ptr++;
    }
    if ( gid && *gid ) {
      basePtr += 1 + sprintf((*arrayPtr = basePtr), "%s=%s", WorkgroupWorkgroupEnv, gid);
      arrayPtr++;
    }
    if ( workDirectory && *workDirectory ) {
      basePtr += 1 + sprintf((*arrayPtr = basePtr), "%s=%s", WorkgroupWorkDirEnv, workDirectory);
      arrayPtr++;

      if ( workDirSw ) {
        basePtr += 1 + sprintf((*arrayPtr = basePtr), "%s=%s", WorkgroupWorkDirSwEnv, workDirSw);
        arrayPtr++;
        free(workDirSw);
      }
      if ( workDirUser ) {
        basePtr += 1 + sprintf((*arrayPtr = basePtr), "%s=%s", WorkgroupWorkDirUserEnv, workDirUser);
        arrayPtr++;
        free(workDirUser);
      }
    }
    *arrayPtr = NULL;
  }
  return newEnviron;
}

//

typedef struct _Workgroup {
  gid_t         gidNumber;
  char          gidNumberStr[12];
  const char*   gid;
  const char*   workDirectory;
} Workgroup;

//

WorkgroupRef
WorkgroupCreateWithGidNumber(
  gid_t   gidNumber
)
{
  Workgroup*      newWorkgroup = NULL;
  struct group*   groupInfo = getgrgid(gidNumber);

  if ( groupInfo ) {
    newWorkgroup = (Workgroup*)malloc(sizeof(Workgroup) + strlen(groupInfo->gr_name) + 1);
    if ( newWorkgroup ) {
      newWorkgroup->gidNumber = groupInfo->gr_gid;
      snprintf(&newWorkgroup->gidNumberStr[0], 12, "%8d  ", groupInfo->gr_gid);

      strcpy((char*)(newWorkgroup->gid = (void*)newWorkgroup + sizeof(Workgroup)), groupInfo->gr_name);

      newWorkgroup->workDirectory = WorkdirMapStackDirectoryForGroup(
                                          WorkdirMapStackGetDefault(),
                                          groupInfo->gr_gid,
                                          groupInfo->gr_name
                                        );
    }
  }
  return (WorkgroupRef)newWorkgroup;
}

//

void
WorkgroupFree(
  WorkgroupRef  aWorkgroup
)
{
  if ( aWorkgroup->workDirectory )
    free((void*)aWorkgroup->workDirectory);
  free(aWorkgroup);
}

//

gid_t
WorkgroupGetGidNumber(
  WorkgroupRef  aWorkgroup
)
{
  return aWorkgroup->gidNumber;
}

//

const char*
WorkgroupGetGidNumberString(
  WorkgroupRef  aWorkgroup
)
{
  return aWorkgroup->gidNumberStr;
}

//

const char*
WorkgroupGetGidString(
  WorkgroupRef  aWorkgroup
)
{
  return aWorkgroup->gid;
}

//

const char*
WorkgroupGetWorkDirectory(
  WorkgroupRef  aWorkgroup
)
{
  return aWorkgroup->workDirectory;
}

//

int
WorkgroupSpawnSubshell(
  WorkgroupRef  aWorkgroup,
  bool          setWorkingDirectory,
  bool          fixupUmask
)
{
  int           rc = 0;

  //
  // Ensure the user isn't already in a shell for this workgroup; we don't really
  // want to spawn one shell after another if already on the appropriate gid:
  //
  if ( getegid() == aWorkgroup->gidNumber ) {
    printf("ERROR:  You are already working in a shell associated with that workgroup.\n\n");
    rc = EACCES;
  }

  //
  // We attempt to change the working directory whether or not the user is
  // already in a workgroup shell:
  //
  if ( setWorkingDirectory && aWorkgroup->workDirectory ) {
    if ( chdir(aWorkgroup->workDirectory) == 0 ) {
      printf("WARNING:  Your working directory has been changed to %s\n\n", aWorkgroup->workDirectory);
    } else {
      printf("ERROR:  Could not change to workgroup directory: %s\n\n", aWorkgroup->workDirectory);
    }
  }
  fflush(stdout);

  //
  // Unless we've already caught an error, try newgrp:
  //
  if ( ! rc ) {
#ifdef WORKGROUP_ENABLE_SPAWN_CLEAN_ENV
    const char*     argv[8] = { "/usr/bin/sg", aWorkgroup->gid, "-c", "/bin/bash -il", NULL };
#else
    const char*     argv[3] = { "/usr/bin/newgrp", aWorkgroup->gid, NULL };
#endif

    if ( fixupUmask ) setenv(WorkgroupFixupUmaskEnv, "YES", 1);
    rc = execve(
            argv[0],
            (char**)argv,
#ifdef WORKGROUP_ENABLE_SPAWN_CLEAN_ENV
            __WorkgroupCreateCleanEnvironment(aWorkgroup->gid, aWorkgroup->workDirectory)
#else
            __WorkgroupCreateEnvironment(aWorkgroup->gid, aWorkgroup->workDirectory)
#endif
          );
  }

  return rc;
}

//

int
WorkgroupExecCommand(
  WorkgroupRef  aWorkgroup,
  bool          setWorkingDirectory,
  bool          fixupUmask,
  const char    *commandString
)
{
  int           rc = 0;

  //
  // We attempt to change the working directory whether or not the user is
  // already in a workgroup shell:
  //
  if ( setWorkingDirectory && aWorkgroup->workDirectory ) {
    if ( chdir(aWorkgroup->workDirectory) == 0 ) {
      printf("WARNING:  The working directory for the command will be %s\n\n", aWorkgroup->workDirectory);
    } else {
      printf("ERROR:  Could not change to workgroup directory: %s\n\n", aWorkgroup->workDirectory);
      rc = errno;
    }
  }
  fflush(stdout);

  //
  // Unless we've already caught an error, try sg:
  //
  if ( ! rc ) {
    const char*     argv[5] = { "/usr/bin/sg", aWorkgroup->gid, "-c", commandString, NULL };

    if ( fixupUmask ) __WorkgroupFixupUmask();
    rc = execve(
            argv[0],
            (char**)argv,
            __WorkgroupCreateEnvironment(aWorkgroup->gid, aWorkgroup->workDirectory)
          );
  }

  return rc;
}
